/**
    @requiredBy apiConnection
 */
module.exports                      = {
    open                            : openTunnel,
    close                           : closeTunnel,
    setInfo                         : setGatewayInfo,
    setLogLvl                       : setLogLvl
};

const winston                       = require("winston")
winston.level                       = "error"

var tls                             = require('tls'),
    net                             = require('net'),
    connections                     = {
        // "main"                          : {
        //     "server"                    : null
        //     "remote-host"               : "",
        //     "remote-port"               : "",
        //     "local-host"                : "",
        //     "local-port"                : "",
        //     "count"                     : 0
        // }
    };

function setGatewayInfo(gatewayOptions, gatewayName){
    var gatewayOptions = gatewayOptions || {};
    if(connections.hasOwnProperty(gatewayName)){
        winston.log("info", "conection -> tunnel -> setGatewayInfo => Can't update the gateway info with name: "+gatewayName+", another gateway is already been created with that name, but different options (URL - HOST)");
    }
    else{
        if(!gatewayOptions.hasOwnProperty('remote-host') || !gatewayOptions.hasOwnProperty("remote-port")) {
            winston.log("err", "Missing parameters");
        }
        else {
            if(gatewayOptions["remote-host"] == '' || gatewayOptions["remote-port"] == 0) {
                winston.log("err", "Invalid parameters");
            }
            else {
                gatewayOptions.count        = (gatewayOptions.count)?gatewayOptions.count:0;
                connections[gatewayName]    = gatewayOptions;
            }
        }
    }
}

function openTunnel(callback, gatewayName){
    var connection                  = connections[gatewayName];
    try {
        if(connection == undefined){
            winston.log("info", "Missing connection");
            throw new Error("Missing connection")
        }
        else {
            winston.log("info", "conection -> tunnel -> create => Tunnel against '"+connection["remote-host"]+":"+connection["remote-port"]+"'");
            if(connection.count == 0){
                winston.log("info", "conection -> tunnel -> create => 'Tunnel does not exist, creation start'");
                connection.count++;
                connection.server           = net.createServer(function(conn){
                    connectFarside(conn, function(err, socket) {
                        if(err){
                            winston.log("error", "conection -> tunnel -> create => 'Connect Farside Fail' => code: "+err.code);
                            callback(err);
                            closeTunnel(gatewayName);
                        }
                        else{
                            socket.pipe(conn);
                            conn.pipe(socket);
                        }
                    }, gatewayName);
                });
                connection.server.listen(connection["local-port"], function(){
                    winston.log("info", "conection -> tunnel -> create -> server => 'Server / Tunel avaible on port: "+connection["local-port"]+"'");
                    callback();
                });
            }
            else{
                winston.log("warn","conection -> tunnel -> create => 'Tunnel exist, executing callback'");
                callback();
            }
        }
    }
    catch(err) {
        winston.log("error", "Something goes wrong: "+ err);
        callback(err);
    }
}

/**
    Se utiliza para realizar la conexion con tls y crear el socket
    @param {object} conn
    @param {function} callback
    @param {string} gatewayName
 */
function connectFarside(conn, callback, gatewayName) {
    try {
        var socket                  = tls.connect({
            host                    : connections[gatewayName]["remote-host"],
            port                    : connections[gatewayName]["remote-port"]
        }, function() {
            winston.log("info", "conection -> tunnel -> connectFarside => 'Tunel Connected'");
            callback(null, socket);
        });

        socket.on('error', function(err){
            winston.log("error", "conection -> tunnel -> connectFarside => 'Socket error' => code: "+err.code);
            callback(err)
            closeTunnel(gatewayName);
        });
    }
    catch(err) {
        winston.log("error", "conection -> tunnel -> connectFarside => 'Unexpected error' => code: "+err.code);
        callback(err);
    }
}

/**
    Cierra la conexion para un gatewayName entregado
    @param {string} gatewayName
 */
function closeTunnel(gatewayName){
    connections[gatewayName].count   = 0;
    try{
        connections[gatewayName].server.close();
    }
    catch(e){
        winston.log("error", "conection -> tunnel -> close => 'Error Closing Tunel' => code: "+e.code);
    }
}

/**
    Set the log level (Winston) in this module
    @param {integer} lvl
 */
function setLogLvl(lvl){
    var logLvls                     = ["error", "warn", "info", "verbose", "debug", "silly"],
        lvl                         = isNaN(lvl)?lvl:logLvls[lvl];

    winston.level                   = lvl;
}
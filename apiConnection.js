/**
*
*   @author  Mobile Dev Team - IBM Argentina <donadlg@ar.ibm.com>
*   @module Api-Connection
*/
module.exports                      = new network();

var connections                     = {
    // auth                            : {user: "", pass: ""}
}

const winston                       = require("winston")
winston.level                       = "error"

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" /*TODO network-a WARNING must update node to avoid this line */

/**
    Constructor de apiConnection
    @require tunel
    @require request
 */
function network(){
    this.tunnel                     = require("./connection/tunnel");
    this.request                    = require("request");
    this.gatewayName                = "main"
}

network.prototype.setGatewayInfo    = function(gatewayInfo, gatewayName){
    this.gatewayName                = gatewayName;
    if(gatewayInfo) {
        this.tunnel.setInfo(gatewayInfo, gatewayName)
    }
}

network.prototype.setConnectionInfo = function(connectionInfo, connectionName){
    connections[connectionName]     = connectionInfo
}



/**
 * Se crea conexión segura
 * @param {function} callback
 */
network.prototype.openTunnel = function(callback) {
    var self = this;
    self.tunnel.open(function(err){
        if(err) {
            winston.log("error", "Error: "+ err.message + " at opening tunnel");
            callback(err);
        }
        else{
            winston.log("info","Tunnel opened");
            callback();
        }
    },this.gatewayName);
}

/**
* Se crea conexion segura para un request
* @param {string} path - ruta que se va a segurilizar
* @param {object} options
* @param {string} type - Tipo de protocolo
* @param {function} callback - Retorna respuesta
 */
network.prototype.gatewayRequest    = function(path, options, type, callback){
    var self                        = this;
    self.tunnel.open(function(err){
        if(!err){
            self.executeRequest(path, options, type, callback);
        }
        else {
            callback(err)
        }
    }, this.gatewayName);
}

/**
*   Se ejecuta el request
*   @param {string} path - Ruta destino
*   @param {object} options
*   @param {string} type - Tipo de protocolo
*   @param {function} callback - Retorna respuesta
 */
network.prototype.executeRequest    = function(path, options, type, callback){
    var baseUrl                     = options.basePath  || "https://localhost:8100" ,
        path                        = path              || "",
        type                        = type              || "soap",
        options                     = options           || {},
        types                       = {
            "soap"                  : {
                method              : "POST",
                body                : options.body      || "",
                headers             : {
                    "Content-Type"  : "application/soap+xml; charset=utf-8"
                }
            },
            "rest"                  : {
                method              : "GET",
                encoding            : "binary",
                json                : true,
                gzip                : true,
                headers             : {}
            }
        },
        opt                         = types[type],
        reqId                       = options.reqId;

    opt.url                         = baseUrl+path;

    delete options.reqId;

    if(type=="soap"){
        opt["headers"]["Content-Length"] = Buffer.byteLength(options.body);
    }

    for(var key in options){
        if(key != "headers"){
            opt[key]                = options[key];
        }
        else{
            for(var header in options["headers"]){
                opt["headers"][header]=options["headers"][header]
            }
        }
    }

    if(opt.basicAuth){
        var basicUser               = connections[this.gatewayName].auth.user || "",
            basicPass               = connections[this.gatewayName].auth.pass || "";

        opt.headers["Authorization"]= "Basic "+(new Buffer(basicUser+":"+basicPass).toString('base64'))
    }

    var timeBefore                  = new Date().getTime(),
        queryParamsIdx              = path.indexOf("?"),
        service                     = (queryParamsIdx!=-1)?path.substr(0, queryParamsIdx):path;

    this.request(opt, function(error/*, response, body*/){
        var timeAfter               = new Date().getTime(),
            segDiff                 = (timeAfter - timeBefore)/1000;

        winston.log("info", "Service: " + service + " - " + reqId + " - Response Time: "+segDiff+" segs"); //req.id
        if(error){
            winston.log("error", "Service-Error: "+service+ " - " + reqId + " - Error-Code: "+error.code);  //req.id
        }

        try{
            callback.apply(null, arguments);
        }
        catch(e){
            winston.log("error", "Service FAIL: "+ reqId +" - "+ service);
            callback.apply(null, [true, {}, ""]);
        }
    });
}

/**
    Set the log level (Winston) in this module and in tunnel.js
    @param {integer} lvl
 */
network.prototype.setLogLvl         = function(lvl){
    var logLvls                     = ["error", "warn", "info", "verbose", "debug", "silly"],
        lvl                         = isNaN(lvl)?lvl:logLvls[lvl];

    winston.level                   = lvl;

    this.tunnel.setLogLvl(lvl);
}

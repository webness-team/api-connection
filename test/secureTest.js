var testSecure 			= require ("./../server/app/commons/secure.js").token;
var chai 				= require ("chai");
var assert 				= chai.assert;
var expect 				= chai.expect;
var jwt 				= require('jsonwebtoken')
var constants		 	= require("./../server/app/commons/commons").constants

chai.use(require('chai-integer'));



var dniLogin 				= "12345678"
var dniLogin2 				= "23456789"
var dniTitular 				= "12345678"
var rol 					= "TIT"
var oneTimeToken 			= true
var tokenToEncrypt 			= "abc"
var tokenEncrypted			= "leZFbNXxoOsHxZX0lj08mA=="
var request 				= {"path":"/api/titulares/accountStatus", "method":"", "id" : "", "secure":"", get : function(attr){
	return request[attr];
}}
var documentSaved 			= "12345678"

describe ("secure", function(){
	var tokenObj 				= testSecure.prototype.create(dniLogin, dniTitular, rol, oneTimeToken)
	var actualDate 				= new Date().getTime()
	describe("#create()" , function(){
		it("is an object?", function(done){
			assert.typeOf(tokenObj, "object", 'is not an Object');
			done();
		})

		it("the object have a defined Properties?", function(done){
			var list = ["token", "expire"]
			list.forEach(function(itm){
				assert.isDefined(tokenObj[itm], 'the object do not have a ' + itm);
			})
			done();
		})

		it("the expire date is greater than actual date?", function(done){
			assert.isAbove(tokenObj.expire, actualDate, 'the expire date is bad');
			done();
		})
	})

	describe("#decode()" , function(){
		it('should decode without error', function(done) {
     		testSecure.prototype.decode(tokenObj.token, function(err, decoded) {
        		if (err) done(err);
        		else done();
        	});
     	});

     	it('should decode return an object', function(done) {
     		testSecure.prototype.decode(tokenObj.token, function(err, decoded) {
				assert.typeOf(decoded, "object", 'is not an Object');
				done();
        	});
     	});


     	it('the object have a defined Properties?', function(done) {
     		testSecure.prototype.decode(tokenObj.token, function(err, decoded) {
				var list = ["dniLogin","dniTitular","rol","expire", "phrase", "iat" ]
				list.forEach(function(itm){
					assert.isDefined(decoded[itm], 'the object do not have a ' + itm);
				});
			done();
        	});
     	});
	})

	describe("#encrypt()" , function(){
		it("is the encryption good?", function(done){
			assert.equal(testSecure.prototype.encrypt(tokenToEncrypt), tokenEncrypted, 'The encryption was not good');
			done();
		})
	})

	describe("#decrypt()" , function(){
		it("is the decryption good?", function(done){
			assert.equal(testSecure.prototype.decrypt(tokenEncrypted), tokenToEncrypt, 'The decrypt was not good');
			done();
		})
	})

	describe("#requiresIntegrityValidation()" , function(){
		it('Require Integrity Validation', function(done) {
	 		assert.equal(testSecure.prototype.requiresIntegrityValidation(request), true, 'The requiresIntegrityValidation was not good');
	 		done();
 		});

 		it('Do not require Integrity Validation', function(done) {
 			request.path = "/api/titulares/promotions"
	 		assert.equal(testSecure.prototype.requiresIntegrityValidation(request), false, 'The requiresIntegrityValidation was not good');
	 		done();
 		});
	})

	describe("#checkValidToken()" , function(){
		it('should checkValidToken without error', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
        		if (validity) done();
        		else done(validity);
        	});
     	});

     	it('should checkValidToken be a valid Token', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
			assert.equal(validity, true, 'The Token is not Valid');
			done();
        	});
     	});

     	it('should checkValidToken be a invalid Token', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin2, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
			assert.equal(validity, false, 'The Token is not Valid');
			done();
        	});
     	});

     	it('should checkValidToken return an object', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
			assert.typeOf(userInfo, "object", 'is not an Object');
			done();
        	});
 		});

     	it('the object have a defined Properties?', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
				var list = ["dniLogin","dniTitular","rol"]
				list.forEach(function(itm){
					assert.isDefined(userInfo[itm], 'the object do not have a ' + itm);
				});
			done();
        	});
     	});
     	it('the object have a defined needToUpdate?', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
				assert.isDefined(needToUpdate, 'the object do not have a needToUpdate');
				done();
        	});
     	});
     	it('should checkValidToken with error', function(done) {
     		testSecure.prototype.checkValidToken(dniLogin2, tokenObj.token, true, function(validity, userInfo, needToUpdate) {
        		if (!validity) done();

        	});
     	});
    });

    describe("#getRandomInt()" , function(){
    	var min 					= 4
		var max 					= 9
    	it("the type is a number?", function(done){
			assert.typeOf(testSecure.prototype.getRandomInt(min,max), "number", 'The number is not a number');
			done();
		})
		it("the type is an integer?", function(done){
			expect(testSecure.prototype.getRandomInt(min,max)).to.be.an.integer();
			done();
		})

		it("the number is greater than min?", function(done){
			assert.isAtLeast(testSecure.prototype.getRandomInt(min,max), min, 'The number was not into the limits');
			done();
		})
		it("the number is less than max?", function(done){
			assert.isAtMost(testSecure.prototype.getRandomInt(min,max), max, 'The number was not into the limits');
			done();
		})
	})

	describe("#randomValueBase64()" , function(){
		var length 			  = 5
		var randomValueBase64 = testSecure.prototype.randomValueBase64(length)
		it("the value has the required length?", function(done){
			assert.lengthOf(randomValueBase64, length, 'The length value is not the correct');
			done();
		})
		it("the value contain only A-z and 0-9?", function(done){
				expect(randomValueBase64).to.not.match(/[^a-zA-Z0-9]+/);
				done();
		})
		it("the value not contain only A-z and 0-9?", function(done){
				expect("HfdR6+4").to.match(/[^a-zA-Z0-9]+/);
				done();
		})
	})

	describe("#getClientToken()" , function(){
		request.secure 		= tokenObj.token
		var clientToken 	= testSecure.prototype.getClientToken(request)
		it("the function should return the token", function(done){
			assert.equal(clientToken, tokenObj.token, 'The clientToken is not the same to the request');
			done();
		})
		it("the function should not return the token", function(done){
			request.secure 		= ""
			var clientToken 	= testSecure.prototype.getClientToken(request)
			assert.notEqual(clientToken, tokenObj.token, 'The clientToken is not the same to the request');
			done();
		})
	})

	describe("#isRequestDataValid()" , function(){
		it('Should return true', function(done) {
	 		assert.equal(testSecure.prototype.isRequestDataValid(dniLogin, documentSaved), true, 'The isRequestDataValid was not good');
	 		done();
 		});
 		it('Should return false', function(done) {
	 		assert.equal(testSecure.prototype.isRequestDataValid(dniLogin2, documentSaved), false, 'The isRequestDataValid was not good');
	 		done();
 		});
	})

	describe("#secureValidation()" , function(){
		var response = {
			  headers : {}
			, append: function(name, val){
				response.headers[name] = val;
			}
		}
		request.method = "POST"
		request.body = {
			"documentoLogin" : "12345678"
		}
		it('Should return true', function(done) {
			testSecure.prototype.secureValidation(request, response, tokenObj, function(validSecure) {
				assert.equal(validSecure, true, 'the validation was wrong');
				done();
        	});
 		});


 		it('Should return a number', function(done) {
 			request.id = "Gsus"
			testSecure.prototype.secureValidation(request, response, tokenObj, function(validSecure) {
				var id = (request.id).split("-")
				var dni = id[0]*1
				assert.isNotNaN(dni, "is not a number");
				done();
        	});
 		});

 		it('updateToken in the header should be undifined', function(done) {
			testSecure.prototype.secureValidation(request, response, tokenObj, function(validSecure) {
				assert.isUndefined(response.headers.updateToken, 'the updateToken was append')
				done();
        	});
 		});

 		it('Should return updateToken', function(done) {
			var cert 						= constants.privateKey
 			var now 						= new Date().getTime()
 			  , expire 						= now + (4*60*1000);

 			var myToken 					= jwt.sign({
				dniLogin					: "12345678"
			  , dniTitular					: "12345678"
			  , rol							: "TIT"
			  , expire						: expire
			  , phrase						: constants.validationPhrase
			} , cert, { algorithm : 'HS256'});

			var myTokenEncrypt 				= testSecure.prototype.encrypt(myToken)

 			testSecure.prototype.secureValidation(request, response, {token : myTokenEncrypt}, function(validSecure) {
 				assert.isDefined(response.headers.updateToken, 'the updateToken was not append')
 			done()
        	});
 		});
	})
	describe("#getClientDocNumber()" , function(){
		request.method = "POST"
		request.body = {
			"documentoLogin" : "12345678"
		}
		it('Should return docNumber with Post', function(done) {
			request.method = "POST"
			request.body = {
				"documentoLogin" : "12345678"
			}
	 		assert.equal(testSecure.prototype.getClientDocNumber(request), dniLogin, 'The docNumber was not good');
	 		done();
 		});

		//  PARA REVISAR JOEL ==> JSON.parse(atob("eyJzY29wZSI6InNlY3VyZSAtIHRlc3RzIiwiaXNzdWVzIjp7Im5hbWluZyI6Ik5vbWJyZXMgcG9jbyBkZXNjcmlwdGl2b3MgPT4gKG1pbiwgbWF4LCBsZW5ndGgpIiwiY292ZXJhZ2UiOiJObyBzZSB2YWxpZGFuIHRvZG9zIGxvcyBjYXNvcyAoJ2NoZWNrVmFsaWRUb2tlbicgc29sbyB2YWxpZGEgY2FzbyB0cnVlLyAncmVxdWlyZXNJbnRlZ3JpdHlWYWxpZGF0aW9uJyBzb2xvIHZhbGlkYSBjYXNvIHRydWUpIn19"))

 		// it('Should return docNumber with Get', function(done) {
			// request.method = "GET"
			// request.params = "/api/titulares/accountSummary/1/12345678"
	 	// 	assert.equal(testSecure.prototype.getClientDocNumber(request), dniLogin, 'The docNumber was not good');
	 	// 	done();
 		// });
	})
})